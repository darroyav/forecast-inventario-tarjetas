# Cargar Paquetes
import matplotlib.pylab as plt
%matplotlib inline
from matplotlib.pylab import rcParams
rcParams['figure.figsize'] = 15, 6
import requests, pandas as pd, numpy as np
from pandas import DataFrame
from io import StringIO
import time, json
from datetime import date
import statsmodels
from statsmodels.tsa.stattools import adfuller, acf, pacf
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.seasonal import seasonal_decompose
from sklearn.metrics import mean_squared_error

# statistics and econometrics
import statsmodels.formula.api as smf
import statsmodels.tsa.api as smt
import statsmodels.api as sm
import scipy.stats as scs
import sys
import types
import types
import pandas as pd

# paquetes autoarima
from statsmodels.tsa.arima_model import ARIMA
import pmdarima as pm
from pmdarima.arima import auto_arima
from sklearn.metrics import mean_squared_error

# Importación del archivo Autoarima
emision = pd.read_csv('D:\SAS\Citizen Data Science\Proyectos Python\Inventarios\TABLA_PROD_MES_201801_201908.csv',sep=',')
emision.columns

# Ajustar formato e indexar fecha
emision['fecha'] = pd.to_datetime(emision['fecha'], format = '%Y-%m-%d')
dates = emision.set_index('fecha')

#### AUTOARIMA
cols = dates.columns
model = []

for i in cols:
    model.append(tuple((pm.auto_arima(dates.loc[:,i], start_p=1, start_q=1,
                          test='adf',       # Use el test adftest para hallar el óptimo de "d"
                          max_p=5, max_q=5, # maximos de p y q
                          m=12,              # frecuencia de la serie
                          d=None,           # Dejar que el modelo determine el "d"
                          seasonal=False,   # No estacional
                          start_P=0,
                          D=0,
                          trace=True,
                          error_action='ignore',
                          suppress_warnings=True,
                          stepwise=True, scoring='mse', scoring_args=None),i)))
n=len(dates.index)

prediction1 = pd.DataFrame(model[0][0].predict(n_periods=n),index=dates.index)
prediction1 = pd.DataFrame(model[0][0].predict(n_periods=n),index=pd.date_range(start="2019-09-01", periods=n, freq="M"))

N = (len(model)/2)
d = np.linspace(-3, N, n, endpoint=True, dtype = int)

for j in d:
    prediction1[cols[j]]= model[j][0].predict(n_periods=n)
prediction1.head()

#### AUTOSARIMA
cols = dates.columns
model = []

for i in cols:
    model.append(tuple((pm.auto_arima(dates.loc[:,i], start_p=1, start_q=1, 
                            max_p=3, max_q=3, 
                            m=12,
                            start_P=0, 
                            seasonal=True, d=1, D=1, trace=True,
                            error_action='ignore',  # don't want to know if an order does not work
                            suppress_warnings=True,  # don't want convergence warnings
                            stepwise=True),i))) # set to stepwise
n=len(dates.index)

prediction2 = pd.DataFrame(model[0][0].predict(n_periods=n),index=dates.index)
prediction2 = pd.DataFrame(model[0][0].predict(n_periods=n),index=pd.date_range(start="2019-07-01", periods=n, freq="M"))

N = (len(model)/2)
d = np.linspace(-3, N, n, endpoint=True, dtype = int)

for j in d:
    prediction2[cols[j]]= model[j][0].predict(n_periods=n)
prediction2
